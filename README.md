# Plot Covid-19

Quelques tracées de données sur la mortalité du Covid-19 (en France ou dans le monde)

- Un sur le monde (cas / morts), données Johns Hopkins CSSE
- Un sur la mortalité journalière en France par département, données INSEE
